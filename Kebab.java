package com.receipt;

import com.sun.jdi.request.StepRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Kebab {
    private final List<Ingredient> listIngredient;

    // CONSTRUCTOR
    public Kebab(List<Ingredient> ingredients)
    {
        this.listIngredient = ingredients;
    }

    public static boolean exist(List<Ingredient> ingredients, String name) {
        return ingredients.stream()
                .anyMatch(i -> i.getNameIngredient().equals(name));
    }

    // METHODS
    // GETTER

    public static List<Ingredient> ingredients() {
        return Stream.of(
                "salade",
                "tomate",
                "oignon",
                "döner",
                "poisson",
                "crevette",
                "surimis",
                "poulet")
                .map(Ingredient::new)
                .collect(Collectors.toList());
    }

    public List<Ingredient> getListIngredient() {
        return listIngredient;
    }

    // GET INGREDIENT
    public static Ingredient getIngredient(List<Ingredient> ings, String name)
    {
        for(int i = 0; i < ings.size(); i++)
        {
            if(ings.get(i).getNameIngredient().equals(name))
            {
                return ings.get(i);
            }
        }
        return null;
    }

    // RETURN TRUE IF THERES ONE MEAT IN THE KEBAB
    public boolean isVeggie()
    {
        return this.listIngredient.stream()
                .map(i -> i.getNameIngredient().toUpperCase(Locale.ROOT))
                .noneMatch(i -> i.equals("DÖNER")
                             || i.equals("POULET")
                             || i.equals("CREVETTE")
                             || i.equals("SURIMIS"));
    }

    public boolean isPescie() {
        boolean hasViande = this.listIngredient.stream()
                .map(i -> i.getNameIngredient().toUpperCase(Locale.ROOT))
                .noneMatch(i -> i.equals("DÖNER") || i.equals("POULET"));

        return hasViande &&
                this.listIngredient.stream()
                        .map(i -> i.getNameIngredient().toUpperCase(Locale.ROOT))
                        .anyMatch(i -> i.equals("CREVETTE") || i.equals("SURIMIS"));
    }

    public static void afficherListeIngs(List<Ingredient> ingredients) {
        ingredients
                .stream()
                .map(Ingredient::getNameIngredient)
                .forEach(System.out::println);
    }

    // ADD
    public void addIngredient(Ingredient i)
    {
        this.listIngredient.add(i);
    }

}
