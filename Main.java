package com.receipt;

import jdk.jshell.Snippet;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static List<Ingredient> addIngs(List<Ingredient> ings) {

        System.out.print("Dans votre kebab: ");
        System.out.println(
                ings.stream()
                        .map(Ingredient::getNameIngredient)
                        .reduce((x,y) -> x + ", " + y)
                        .orElse("")
        );
        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez choisir un ingrédient");
        Kebab.ingredients()
                .forEach(i ->
                        System.out.println(i.getNameIngredient()));
        System.out.print("votre choix: ");
        String name = scanner.nextLine();
        if (Kebab.getIngredient(Kebab.ingredients(), name) == null) {
            System.out.println("ingredient non trouvé, veuillez ressayer...");
            return addIngs(ings);
        }

        if (Kebab.exist(ings, name)) {
            System.out.println("Malheuresement cette ingrédients est déjà présent...");
            return addIngs(ings);
        }

        ings.add(new Ingredient(name));
        System.out.println("Souhaitez vous ajouter un autre ingrédient ? (o/n)");
        if (scanner.nextLine().equals("o")) return addIngs(ings);
        return ings;
    }

    public static List<Ingredient> retirer(List<Ingredient> ingredients) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Souhaitez vous retirer un ingrédient ?");
        System.out.println("Si oui tapez o");

        if (scanner.nextLine().equals("o")) {
            ingredients.forEach(i -> System.out.println(i.getNameIngredient()));
            String name = scanner.nextLine();
        }

        return null;
    }

    public static void main(String[] args) {
	// write your code here
        List<Ingredient> ingredients = addIngs(new ArrayList<>());
        Kebab k = new Kebab(ingredients);

        System.out.println("votre liste d'ingrédients");
        System.out.println("pain");
        Kebab.afficherListeIngs(ingredients);

        if (k.isVeggie()) {
            System.out.println("il s'agit d'un kebab veggie");
        } else {
            System.out.println("ce kebab n'est pas veggie");
        }

        if (k.isPescie()) {
            System.out.println("il s'agit d'un specie");
        } else {
            System.out.println("ce n'est pas un pescie");
        }
    }
}
