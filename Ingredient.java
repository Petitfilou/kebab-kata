package com.receipt;

public class Ingredient {

    private String nameIngredient;

    public Ingredient(String n)
    {
        nameIngredient=n;
    }

    public String getNameIngredient()
    {
        return this.nameIngredient;
    }
}
